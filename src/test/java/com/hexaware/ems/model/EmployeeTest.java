package com.hexaware.ems.model;

import static org.junit.Assert.*;
import java.time.LocalDate;
import org.junit.Test;



public class EmployeeTest {

	Employee emp = new Employee("hemanth","h@gmail.com",LocalDate.of(2020, 05, 05));

	@Test
	public void testempname() {
		emp.setName("arjun");
		assertEquals("arjun", emp.getName());
	}
	
	@Test
	public void testemailid() {
		emp.setEmailId("goutam@xmail.com");
		assertEquals("batmobile@g.com", emp.getEmailId());
	}
	
	@Test
	public void testempdoj() {
		emp.setDate(LocalDate.of(2005, 03, 05));
		assertEquals(LocalDate.of(2005, 03, 05), emp.getDate());
	}
	
	
	@Test
	public void testempid() {
		emp.setEmpId(2005);
		assertEquals(20007, emp.getEmpId());
	}
	
	@Test
	public void testleavebalance() {
		emp.setLeaveBalance(60000);;
		assertEquals(60000, emp.getLeaveBalance());
	}
	
	
}
