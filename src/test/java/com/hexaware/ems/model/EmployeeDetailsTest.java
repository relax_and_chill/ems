package com.hexaware.ems.model;
import java.time.*;

//import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.*;
import org.junit.Test;
	
	public class EmployeeDetailsTest {
		@Test
		public void testSetEmpId() {
			Employee emp=new Employee("sasas","dasds",LocalDate.of(2020, 03, 20));
			emp.setEmpId(1000);
			
			assertEquals(1000, emp.getEmpId());
		}
}
