package com.hexaware.ems.model;


import java.time.LocalDate;

public class Employee {
	private long empId;
	private Employee managerID;
	private String name;
	private String emailId;
	private LocalDate date;
	private int leaveBalance;
	private String empName;
	
	public Employee(String name,String emailId, LocalDate date) {
		this.name=name;
		this.emailId=emailId;
		this.date=date;
	}
	
	public Employee(long empId, String empName) {
		this.empId= empId;
		this.empName= empName;
	}
	
	public Employee(long employeeId, String employeename,LocalDate date) {
		this.empId= employeeId;
		this.name= employeename;
		this.date=date;

	}


	public Employee getManagerID() {
		return managerID;
	}

	public void setManagerID(Employee managerID) {
		this.managerID = managerID;
	}

	public long getEmpId() {
		return empId;
	}
	public void setEmpId(long empId) {
		this.empId = empId;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result + (int) (empId ^ (empId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (emailId == null) {
			if (other.emailId != null)
				return false;
		} else if (!emailId.equals(other.emailId))
			return false;
		if (empId != other.empId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
	

	public int getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(int leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", managerID=" + managerID + ", name=" + name + ", emailId=" + emailId
				+ ", date=" + date + ", leaveBalance=" + leaveBalance + "]";
	}


	
}
