package com.hexaware.ems.service;
import java.util.Set;
import com.hexaware.ems.model.Employee;

public interface EmployeeService {
	
	public Employee save(Employee employeeDetails);
	
	public Set<Employee> findAll();
	
	public Employee findById(long empId);
	
	public void deleteById(long empId);
	

}
