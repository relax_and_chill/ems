package com.hexaware.ems.dao;

import java.sql.Statement;
import java.time.LocalDate;
import java.sql.*;
import java.util.Set;
import com.hexaware.ems.model.Employee;
import com.hexaware.ems.util.JDBCUtil;

public class EmployeeDAOImpl implements  EmployeeDAO{
	private final JDBCUtil jdbcUtil;
	 public EmployeeDAOImpl(JDBCUtil jdbcUtil) {
		this.jdbcUtil=jdbcUtil;
	}
	 
	@Override
	public Employee save(Employee employeeDetails) {
		 Connection connection = null;
		 try {
		 connection = jdbcUtil.getDBConnection("root", "welcome");
         Statement statement = (Statement) connection.createStatement();
         statement.executeQuery("insert into employee values ("+employeeDetails.getEmpId()+","+
                        employeeDetails.getName()+","+employeeDetails.getEmailId()+","+employeeDetails.getDate()+");");
	}
		 catch(SQLException e) {
			 e.printStackTrace();
		 }
		 return null;
	}
	

	@Override
	public Set<Employee> findAll() {

		return null;
	}

	@Override
	public Employee findById(long empId) {
		Connection connection=null;
		try {
			connection=jdbcUtil.getDBConnection("root", "welcome");
			Statement statement=connection.createStatement();
			ResultSet resultSet=statement.executeQuery("select * from employee where emp_id=" + empId);
			if(resultSet.next()) {
				long employeeId=resultSet.getInt(1);
				String empName=resultSet.getString(2);
				String City=resultSet.getString(2);
				Employee employee=new Employee(employeeId,empName);
				return employee;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			}
		return null;
	}

	
	@Override
	public Employee deleteById(long empId) {
	     Connection connection = null;
	     try {
	    	 connection = jdbcUtil.getDBConnection("root", "welcome");
	            Statement statement = connection.createStatement();
	            ResultSet resultSet = statement.executeQuery("delete from employee where e_id=" + empId);
	            /*if (resultSet.next()) {
	         
	                long employeeId = resultSet.getInt(1);
	                String employeename = resultSet.getString(2);
	                String empemailId = resultSet.getString(3);
	                LocalDate doj = resultSet.getDate(4).toLocalDate();
	                Employee employee = new Employee(employeename,empemailId,doj);
	                return employee;
	                
	            }*/
	     } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return null;
	}

}
