package com.hexaware.ems.dao;
import com.hexaware.ems.model.*;
import java.util.Set;

public interface EmployeeDAO {
	
	public Employee save(Employee employeeDetails);
	
	public Set<Employee> findAll();
	
	public Employee findById(long empId);
	
	public Employee deleteById(long empId);
	
	
	

}
